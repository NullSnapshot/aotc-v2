local ObjectManager = require("managers.object.object_manager")

HyperdriveResearchFacilityScreenPlay = ScreenPlay:new {
	numberOfActs = 1,

	screenplayName = "HyperdriveResearchFacilityScreenPlay",

	buildingID = 479815
}

registerScreenPlay("HyperdriveResearchFacilityScreenPlay", true)

function HyperdriveResearchFacilityScreenPlay:start()
	if isZoneEnabled("rori") then
		local pBuilding = getSceneObject(self.buildingID)
		createObserver(FACTIONBASEFLIPPED, "HyperdriveResearchFacilityScreenPlay", "flipBase", pBuilding)

		if getRandomNumber(100) >= 50 then
			self:spawnRebels(pBuilding)
		else
			self:spawnImperials(pBuilding)
		end
	end
end

function HyperdriveResearchFacilityScreenPlay:flipBase(pBuilding)
	if (pBuilding == nil) then
		return 1
	end

	BuildingObject(pBuilding):destroyChildObjects()

	if BuildingObject(pBuilding):getFaction() == FACTIONIMPERIAL then
		self:spawnRebels(pBuilding)
	elseif BuildingObject(pBuilding):getFaction() == FACTIONREBEL then
		self:spawnImperials(pBuilding)
	end

	return 0
end

function HyperdriveResearchFacilityScreenPlay:spawnImperials(pBuilding)
		BuildingObject(pBuilding):initializeStaticGCWBase(FACTIONIMPERIAL)

		--imperial decorations
		BuildingObject(pBuilding):spawnChildSceneObject("object/tangible/gcw/flip_banner_onpole_imperial.iff", -1130.5, 77.2, 4532.2, 0, 1, 0, 0, 0)
		BuildingObject(pBuilding):spawnChildSceneObject("object/tangible/gcw/flip_banner_onpole_imperial.iff", -1137.0, 77.2, 4532.2, 0, 1, 0, 0, 0)
		BuildingObject(pBuilding):spawnChildSceneObject("object/tangible/gcw/flip_banner_onpole_imperial.iff", -1153.8, 77.2, 4510.9, 0, 1, 0, 0, 0)
		BuildingObject(pBuilding):spawnChildSceneObject("object/tangible/gcw/flip_banner_onpole_imperial.iff", -1117.3, 77.2, 4510.9, 0, 1, 0, 0, 0)
		BuildingObject(pBuilding):spawnChildSceneObject("object/tangible/gcw/flip_banner_onpole_imperial.iff", -1133.9, 77.2, 4511.4, 0, 1, 0, 0, 0)
		BuildingObject(pBuilding):spawnChildSceneObject("object/tangible/gcw/flip_banner_onpole_imperial.iff", -1145.8, 89.0, 4552.4, 0, 1, 0, 0, 0)
		BuildingObject(pBuilding):spawnChildSceneObject("object/tangible/gcw/flip_banner_onpole_imperial.iff", -1136.4, 78.1, 4584.3, 0, 1, 0, 0, 0)

		BuildingObject(pBuilding):spawnChildSceneObject("object/tangible/gcw/flip_banner_onpole_imperial.iff", 6.7, -12.0, 41.4, 479820, 1, 0, 0, 0)
		BuildingObject(pBuilding):spawnChildSceneObject("object/tangible/gcw/flip_banner_onpole_imperial.iff", 0.2, -12.0, 41.4, 479820, 1, 0, 0, 0)

		--imperials outside
		BuildingObject(pBuilding):spawnChildCreature("imperial_trooper", 60, -1100, 76.0, 4520, 0, 0)
		BuildingObject(pBuilding):spawnChildCreature("imperial_trooper", 60, -1100, 76.0, 4524, 0, 0)
		BuildingObject(pBuilding):spawnChildCreature("imperial_trooper", 60, -1100, 76.0, 4528, 0, 0)
		BuildingObject(pBuilding):spawnChildCreature("imperial_trooper", 60, -1130.5, 77.2, 4511.5, 180, 0)
		BuildingObject(pBuilding):spawnChildCreature("imperial_trooper", 60, -1136.9, 77.2, 4511.5, 180, 0)

		BuildingObject(pBuilding):spawnChildCreature("rep_at_xt", 300, getRandomNumber(12) + -1142.5, 77.7, getRandomNumber(12) + 4586.5, getRandomNumber(180) + -90, 0)

		BuildingObject(pBuilding):spawnChildCreature("clonetrooper_501", 60, getRandomNumber(32) + -1150.0, 77.2, getRandomNumber(12) + 4533.5, getRandomNumber(180) + -90, 0)
		BuildingObject(pBuilding):spawnChildCreature("clonetrooper_501", 60, getRandomNumber(32) + -1150.0, 77.2, getRandomNumber(12) + 4533.5, getRandomNumber(180) + -90, 0)
		BuildingObject(pBuilding):spawnChildCreature("clonetrooper_501", 60, getRandomNumber(32) + -1150.0, 77.2, getRandomNumber(12) + 4533.5, getRandomNumber(180) + -90, 0)
		BuildingObject(pBuilding):spawnChildCreature("clonetrooper_501", 60, getRandomNumber(32) + -1150.0, 77.2, getRandomNumber(12) + 4533.5, getRandomNumber(180) + -90, 0)
		BuildingObject(pBuilding):spawnChildCreature("clone_commando", 60, getRandomNumber(32) + -1150.0, 77.2, getRandomNumber(12) + 4533.5, getRandomNumber(180) + -90, 0)
		BuildingObject(pBuilding):spawnChildCreature("clone_commando", 60, getRandomNumber(32) + -1150.0, 77.2, getRandomNumber(12) + 4533.5, getRandomNumber(180) + -90, 0)
		BuildingObject(pBuilding):spawnChildCreature("clone_commando", 60, getRandomNumber(32) + -1150.0, 77.2, getRandomNumber(12) + 4533.5, getRandomNumber(180) + -90, 0)

		BuildingObject(pBuilding):spawnChildCreature("imperial_trooper", 60, -1185, 75.9, 4510, 45, 0)
		BuildingObject(pBuilding):spawnChildCreature("imperial_trooper", 60, -1182, 75.9, 4513, 45, 0)
		BuildingObject(pBuilding):spawnChildCreature("imperial_trooper", 60, -1179, 75.9, 4516, 45, 0)
		BuildingObject(pBuilding):spawnChildCreature("imperial_trooper", 60, -1176, 75.9, 4519, 45, 0)

		--imperials inside
		BuildingObject(pBuilding):spawnChildCreature("imperial_trooper", 60, 0.0, 0.3, 8.9, 0,  479816)
		BuildingObject(pBuilding):spawnChildCreature("imperial_trooper", 60, 0.0, 0.3, 6.4, 0,  479816)

		BuildingObject(pBuilding):spawnChildCreature("clonetrooper_major_501", 60, -3.2, 0.3, -1.1, -90,  479817)

		BuildingObject(pBuilding):spawnChildCreature("clone_commando", 60, 3.7, 0.3, -4.3, -90,  479818)

		BuildingObject(pBuilding):spawnChildCreature("imperial_trooper", 60, 2.2, -12.0, 21.2, 90,  479819)
		BuildingObject(pBuilding):spawnChildCreature("imperial_trooper", 60, 4.8, -12.0, 21.2, -90,  479819)

		BuildingObject(pBuilding):spawnChildCreature("rep_jedi_knight", 60, 2.4, -12.0, 40.3, 0,  479820)
		BuildingObject(pBuilding):spawnChildCreature("imperial_private", 60, 4.4, -12.0, 40.3, 0,  479820)
		BuildingObject(pBuilding):spawnChildCreature("clonetrooper_501", 60, -2.4, -12.0, 37.1, 90,  479820)
		BuildingObject(pBuilding):spawnChildCreature("clonetrooper_501", 60, -2.4, -12.0, 34.1, 90,  479820)
		BuildingObject(pBuilding):spawnChildCreature("clonetrooper_501", 60, -2.4, -12.0, 31.1, 90,  479820)
		BuildingObject(pBuilding):spawnChildCreature("clonetrooper_501", 60, -2.4, -12.0, 28.1, 90,  479820)
		BuildingObject(pBuilding):spawnChildCreature("clonetrooper_501", 60, 10.2, -12.0, 33.1, -90,  479820)
		BuildingObject(pBuilding):spawnChildCreature("clonetrooper_501", 60, 10.2, -12.0, 36.1, -90,  479820)
		BuildingObject(pBuilding):spawnChildCreature("clonetrooper_sniper_501", 60, 25.8, -12.0, 50.7, -155,  479820)
		BuildingObject(pBuilding):spawnChildCreature("clonetrooper_sniper_501", 60, 22.3, -12.0, 40.7, -55,  479820)
		BuildingObject(pBuilding):spawnChildCreature("clonetrooper_sniper_501", 60, 14.8, -12.0, 44.3, 55,  479820)
		BuildingObject(pBuilding):spawnChildCreature("clonetrooper_sniper_501", 60, 14.3, -12.0, 52.8, 180,  479820)
		BuildingObject(pBuilding):spawnChildCreature("clonetrooper_major_501", 60, 10.1, -12.0, 47.2, -90,  479820)
		BuildingObject(pBuilding):spawnChildCreature("imperial_warrant_officer_ii", 60, -1.5, -12.0, 62.7, 90,  479820)
		BuildingObject(pBuilding):spawnChildCreature("imperial_warrant_officer_ii", 60, 1.9, -12.0, 61.2, 35,  479820)
		BuildingObject(pBuilding):spawnChildCreature("imperial_warrant_officer_ii", 60, 3.6, -12.0, 53.6, 180,  479820)
		BuildingObject(pBuilding):spawnChildCreature("clone_commando", 60, -18.4, -12.0, 53.6, 180,  479820)
		BuildingObject(pBuilding):spawnChildCreature("clone_commando", 60, -15.4, -12.0, 53.6, 180,  479820)
		BuildingObject(pBuilding):spawnChildCreature("clone_commando", 60, -12.4, -12.0, 53.6, 180,  479820)
		BuildingObject(pBuilding):spawnChildCreature("clone_commando", 60, -9.4, -12.0, 53.6, 180,  479820)
		BuildingObject(pBuilding):spawnChildCreature("imperial_sergeant", 60, -17.5, -12.0, 43.2, 40,  479820)

		BuildingObject(pBuilding):spawnChildCreature("imperial_trooper", 60, -41.8, -20.0, 48.3, 180, 479821)
		BuildingObject(pBuilding):spawnChildCreature("imperial_trooper", 60, -41.8, -20.0, 45.7, 0, 479821)

		BuildingObject(pBuilding):spawnChildCreature("clonetrooper_major_501", 60, -49.0, -20.0, 28.0, 90, 479822)
		BuildingObject(pBuilding):spawnChildCreature("imperial_second_lieutenant", 60, -67.9, -20.0, 24.4, 175, 479822)
		BuildingObject(pBuilding):spawnChildCreature("imperial_second_lieutenant", 60, -67.2, -20.0, 69.9, -24, 479822)

		BuildingObject(pBuilding):spawnChildCreature("imperial_lance_corporal", 60, -50.5, -20.0, 18.7, 24, 479823)
		BuildingObject(pBuilding):spawnChildCreature("imperial_lance_corporal", 60, -49.0, -20.0, 17.5, 64, 479823)
		BuildingObject(pBuilding):spawnChildCreature("imperial_lance_corporal", 60, -48.7, -20.0, 9.2, 120, 479823)
		BuildingObject(pBuilding):spawnChildCreature("imperial_lance_corporal", 60, -50.5, -20.0, 7.2, 160, 479823)
		BuildingObject(pBuilding):spawnChildCreature("clonetrooper_major_501", 60, -60.7, -20.0, 11.7, -90, 479823)
		BuildingObject(pBuilding):spawnChildCreature("rep_jedi_knight", 300, -60.7, -20.0, 14.0, -90, 479823)
		BuildingObject(pBuilding):spawnChildCreature("imperial_surface_marshall", 60, -59.1, -20.0, 6.9, 180, 479823)
		BuildingObject(pBuilding):spawnChildCreature("imperial_second_lieutenant", 60, -62.6, -20.0, 6.9, 180, 479823)
		BuildingObject(pBuilding):spawnChildCreature("imperial_surface_marshall", 60, -66.1, -20.0, 6.9, 180, 479823)
		BuildingObject(pBuilding):spawnChildCreature("imperial_master_sergeant", 60, -74.6, -20.0, 7.3, -145, 479823)
		BuildingObject(pBuilding):spawnChildCreature("imperial_master_sergeant", 60, -76.2, -20.0, 8.8, -125, 479823)
		BuildingObject(pBuilding):spawnChildCreature("clonetrooper_bombardier_501", 60, -78.2, -20.0, 12.0, 90, 479823)
		BuildingObject(pBuilding):spawnChildCreature("clonetrooper_bombardier_501", 60, -78.2, -20.0, 14.4, 90, 479823)
		BuildingObject(pBuilding):spawnChildCreature("imperial_private", 60, -75.6, -20.0, 17.9, -45, 479823)
		BuildingObject(pBuilding):spawnChildCreature("imperial_surface_marshall", 60, -59.1, -20.0, 19.0, 0, 479823)
		BuildingObject(pBuilding):spawnChildCreature("imperial_second_lieutenant", 60, -62.6, -20.0, 19.0, 0, 479823)
		BuildingObject(pBuilding):spawnChildCreature("imperial_surface_marshall", 60, -66.1, -20.0, 19.0, 0, 479823)

		BuildingObject(pBuilding):spawnChildCreature("imperial_medic", 60, -77.1, -20.0, 74.4, 45, 479824)
		BuildingObject(pBuilding):spawnChildCreature("imperial_medic", 60, -53.8, -20.0, 86.4, 90, 479824)
		BuildingObject(pBuilding):spawnChildCreature("imperial_medic", 60, -50.2, -20.0, 79.8, 135, 479824)
		BuildingObject(pBuilding):spawnChildCreature("imperial_medic", 60, -69.2, -20.0, 77.3, -90, 479824)

		BuildingObject(pBuilding):spawnChildCreature("clonetrooper_medic_501", 60, 36.5, -12.0, 52.2, 0, 479825)
		BuildingObject(pBuilding):spawnChildCreature("clonetrooper_captain_501", 60, 34.9, -12.0, 52.2, 0, 479825)
		BuildingObject(pBuilding):spawnChildCreature("clonetrooper_sniper_501", 60, 36.5, -12.0, 57.2, 0, 479825)
		BuildingObject(pBuilding):spawnChildCreature("clonetrooper_sniper_501", 60, 34.9, -12.0, 57.2, 0, 479825)
		BuildingObject(pBuilding):spawnChildCreature("clonetrooper_squad_leader_501", 60, 36.5, -12.0, 62.2, 0, 479825)
		BuildingObject(pBuilding):spawnChildCreature("clonetrooper_medic_501", 60, 34.9, -12.0, 62.2, 0, 479825)

		BuildingObject(pBuilding):spawnChildCreature("droid_corps_junior_technician", 60, 49.3, -12.0, 87.8, 180, 479826)
		BuildingObject(pBuilding):spawnChildCreature("droid_corps_junior_technician", 60, 56.7, -12.0, 87.8, 180, 479826)
		BuildingObject(pBuilding):spawnChildCreature("droid_corps_junior_technician", 60, 63.3, -12.0, 87.8, 180, 479826)
		BuildingObject(pBuilding):spawnChildCreature("droid_corps_junior_technician", 60, 70.0, -12.0, 87.8, 180, 479826)
		BuildingObject(pBuilding):spawnChildCreature("droid_corps_junior_technician", 60, 49.3, -12.0, 78.4, 0, 479826)
		BuildingObject(pBuilding):spawnChildCreature("droid_corps_junior_technician", 60, 56.7, -12.0, 78.4, 0, 479826)
		BuildingObject(pBuilding):spawnChildCreature("droid_corps_junior_technician", 60, 63.3, -12.0, 78.4, 0, 479826)
		BuildingObject(pBuilding):spawnChildCreature("droid_corps_junior_technician", 60, 70.0, -12.0, 78.4, 0, 479826)

		BuildingObject(pBuilding):spawnChildCreature("imperial_warrant_officer_ii", 60, 50.4, -12.0, 62.6, 82, 479827)
		BuildingObject(pBuilding):spawnChildCreature("imperial_warrant_officer_ii", 60, 50.4, -12.0, 55.8, 122, 479827)

		BuildingObject(pBuilding):spawnChildCreature("clone_commando", 60, 36.4, -20.0, 111.8, 180, 479828)
		BuildingObject(pBuilding):spawnChildCreature("clone_commando", 60, 34.6, -20.0, 111.8, 180, 479828)

		BuildingObject(pBuilding):spawnChildCreature("imperial_trooper", 60, 52.2, -12.0, -4.2, 0, 479829)
		BuildingObject(pBuilding):spawnChildCreature("imperial_trooper", 60, 49.2, -12.0, -4.2, 0, 479829)
		BuildingObject(pBuilding):spawnChildCreature("imperial_trooper", 60, 46.2, -12.0, -4.2, 0, 479829)
		BuildingObject(pBuilding):spawnChildCreature("imperial_trooper", 60, 43.2, -12.0, -4.2, 0, 479829)
		BuildingObject(pBuilding):spawnChildCreature("imperial_trooper", 60, 36.2, -12.0, -4.2, 0, 479829)
		BuildingObject(pBuilding):spawnChildCreature("imperial_trooper", 60, 33.2, -12.0, -4.2, 0, 479829)
		BuildingObject(pBuilding):spawnChildCreature("imperial_trooper", 60, 30.2, -12.0, -4.2, 0, 479829)
		BuildingObject(pBuilding):spawnChildCreature("imperial_trooper", 60, 27.2, -12.0, -4.2, 0, 479829)
		BuildingObject(pBuilding):spawnChildCreature("imperial_trooper", 60, 20.2, -12.0, -4.2, 0, 479829)
		BuildingObject(pBuilding):spawnChildCreature("imperial_trooper", 60, 17.2, -12.0, -4.2, 0, 479829)
		BuildingObject(pBuilding):spawnChildCreature("imperial_trooper", 60, 52.2, -12.0, 26.0, 180, 479829)
		BuildingObject(pBuilding):spawnChildCreature("imperial_trooper", 60, 49.2, -12.0, 26.0, 180, 479829)
		BuildingObject(pBuilding):spawnChildCreature("imperial_trooper", 60, 46.2, -12.0, 26.0, 180, 479829)
		BuildingObject(pBuilding):spawnChildCreature("imperial_trooper", 60, 43.2, -12.0, 26.0, 180, 479829)
		BuildingObject(pBuilding):spawnChildCreature("imperial_trooper", 60, 36.2, -12.0, 26.0, 180, 479829)
		BuildingObject(pBuilding):spawnChildCreature("imperial_trooper", 60, 33.2, -12.0, 26.0, 180, 479829)
		BuildingObject(pBuilding):spawnChildCreature("imperial_trooper", 60, 30.2, -12.0, 26.0, 180, 479829)
		BuildingObject(pBuilding):spawnChildCreature("imperial_trooper", 60, 27.2, -12.0, 26.0, 180, 479829)
		BuildingObject(pBuilding):spawnChildCreature("imperial_trooper", 60, 20.2, -12.0, 26.0, 180, 479829)
		BuildingObject(pBuilding):spawnChildCreature("imperial_trooper", 60, 17.2, -12.0, 26.0, 180, 479829)
		BuildingObject(pBuilding):spawnChildCreature("imperial_warrant_officer_i", 60, 61.6, -12.0, 16.3, 90, 479829)
		BuildingObject(pBuilding):spawnChildCreature("imperial_warrant_officer_i", 60, 61.6, -12.0, 5.5, 90, 479829)
		BuildingObject(pBuilding):spawnChildCreature("imperial_moff", 60, 52.0, -12.0, 10.0, -90, 479829)
		BuildingObject(pBuilding):spawnChildCreature("imperial_high_general", 60, 52.0, -12.0, 11.5, -90, 479829)
		BuildingObject(pBuilding):spawnChildCreature("imperial_high_general", 60, 52.0, -12.0, 8.6, -90, 479829)
		BuildingObject(pBuilding):spawnChildCreature("imperial_general", 60, 48.6, -12.0, 15.5, 180, 479829)
		BuildingObject(pBuilding):spawnChildCreature("clonetrooper_major_501", 60, 46.0, -12.0, 15.5, 180, 479829)
		BuildingObject(pBuilding):spawnChildCreature("rep_jedi_knight", 60, 41.7, -12.0, 15.5, 180, 479829)
		BuildingObject(pBuilding):spawnChildCreature("clonetrooper_major_501", 60, 39.2, -12.0, 15.5, 180, 479829)
		BuildingObject(pBuilding):spawnChildCreature("imperial_general", 60, 47.6, -12.0, 4.7, 0, 479829)
		BuildingObject(pBuilding):spawnChildCreature("imperial_lieutenant_general", 60, 44.1, -12.0, 4.7, 0, 479829)
		BuildingObject(pBuilding):spawnChildCreature("imperial_lieutenant_general", 60, 41.4, -12.0, 4.7, 0, 479829)
		BuildingObject(pBuilding):spawnChildCreature("clonetrooper_major_501", 60, 38.9, -12.0, 4.7, 0, 479829)
		BuildingObject(pBuilding):spawnChildCreature("rep_jedi_knight", 60, 33.0, -12.0, 10.0, 90, 479829)

		BuildingObject(pBuilding):spawnChildCreature("imperial_trooper", 60, getRandomNumber(14) + -23.7, -20.0, getRandomNumber(13) + -4.2, getRandomNumber(360), 479831)
		BuildingObject(pBuilding):spawnChildCreature("imperial_trooper", 60, getRandomNumber(14) + -23.7, -20.0, getRandomNumber(13) + -4.2, getRandomNumber(360), 479831)
		BuildingObject(pBuilding):spawnChildCreature("imperial_trooper", 60, getRandomNumber(14) + -23.7, -20.0, getRandomNumber(13) + -4.2, getRandomNumber(360), 479831)
		BuildingObject(pBuilding):spawnChildCreature("imperial_trooper", 60, getRandomNumber(14) + -23.7, -20.0, getRandomNumber(13) + -4.2, getRandomNumber(360), 479831)
		BuildingObject(pBuilding):spawnChildCreature("clonetrooper_501", 60, getRandomNumber(14) + -23.7, -20.0, getRandomNumber(13) + -4.2, getRandomNumber(360), 479831)
		BuildingObject(pBuilding):spawnChildCreature("clonetrooper_501", 60, getRandomNumber(14) + -23.7, -20.0, getRandomNumber(13) + -4.2, getRandomNumber(360), 479831)
		BuildingObject(pBuilding):spawnChildCreature("clonetrooper_501", 60, getRandomNumber(14) + -23.7, -20.0, getRandomNumber(13) + -4.2, getRandomNumber(360), 479831)
		BuildingObject(pBuilding):spawnChildCreature("clonetrooper_501", 60, getRandomNumber(14) + -23.7, -20.0, getRandomNumber(13) + -4.2, getRandomNumber(360), 479831)

		BuildingObject(pBuilding):spawnChildCreature("clonetrooper_501", 60, -31.2, -20.0, 40.0, 180, 479833)
		BuildingObject(pBuilding):spawnChildCreature("clonetrooper_501", 60, -31.2, -20.0, 45.0, 180, 479833)
		BuildingObject(pBuilding):spawnChildCreature("clonetrooper_501", 60, -31.2, -20.0, 50.0, 180, 479833)
		BuildingObject(pBuilding):spawnChildCreature("clonetrooper_501", 60, -31.2, -20.0, 55.0, 180, 479833)
		BuildingObject(pBuilding):spawnChildCreature("clonetrooper_501", 60, -29.8, -20.0, 40.0, 180, 479833)
		BuildingObject(pBuilding):spawnChildCreature("clonetrooper_501", 60, -29.8, -20.0, 45.0, 180, 479833)
		BuildingObject(pBuilding):spawnChildCreature("clonetrooper_501", 60, -29.8, -20.0, 50.0, 180, 479833)
		BuildingObject(pBuilding):spawnChildCreature("clonetrooper_501", 60, -29.8, -20.0, 55.0, 180, 479833)

		BuildingObject(pBuilding):spawnChildCreature("droid_corps_junior_technician", 60, -8.0, -20.0, 102.1, 110, 479834)
		BuildingObject(pBuilding):spawnChildCreature("droid_corps_junior_technician", 60, -37.4, -20.0, 99.9, -55, 479834)

		BuildingObject(pBuilding):spawnChildCreature("clonetrooper_medic_501", 60, getRandomNumber(12) + -9.5, -20.0, getRandomNumber(12) + 65.3, getRandomNumber(360), 479836)
		BuildingObject(pBuilding):spawnChildCreature("clonetrooper_medic_501", 60, getRandomNumber(12) + -9.5, -20.0, getRandomNumber(12) + 65.3, getRandomNumber(360), 479836)
		BuildingObject(pBuilding):spawnChildCreature("clonetrooper_medic_501", 60, getRandomNumber(12) + -9.5, -20.0, getRandomNumber(12) + 65.3, getRandomNumber(360), 479836)
		BuildingObject(pBuilding):spawnChildCreature("clonetrooper_medic_501", 60, getRandomNumber(12) + -9.5, -20.0, getRandomNumber(12) + 65.3, getRandomNumber(360), 479836)
		BuildingObject(pBuilding):spawnChildCreature("clonetrooper_medic_501", 60, getRandomNumber(12) + -9.5, -20.0, getRandomNumber(12) + 65.3, getRandomNumber(360), 479836)
end

function HyperdriveResearchFacilityScreenPlay:spawnRebels(pBuilding)
		BuildingObject(pBuilding):initializeStaticGCWBase(FACTIONREBEL)

		--rebel decorations
		BuildingObject(pBuilding):spawnChildSceneObject("object/tangible/gcw/flip_banner_onpole_rebel.iff", -1130.5, 77.2, 4532.2, 0, 1, 0, 0, 0)
		BuildingObject(pBuilding):spawnChildSceneObject("object/tangible/gcw/flip_banner_onpole_rebel.iff", -1137.0, 77.2, 4532.2, 0, 1, 0, 0, 0)
		BuildingObject(pBuilding):spawnChildSceneObject("object/tangible/gcw/flip_banner_onpole_rebel.iff", -1153.8, 77.2, 4510.9, 0, 1, 0, 0, 0)
		BuildingObject(pBuilding):spawnChildSceneObject("object/tangible/gcw/flip_banner_onpole_rebel.iff", -1117.3, 77.2, 4510.9, 0, 1, 0, 0, 0)
		BuildingObject(pBuilding):spawnChildSceneObject("object/tangible/gcw/flip_banner_onpole_rebel.iff", -1133.9, 77.2, 4511.4, 0, 1, 0, 0, 0)
		BuildingObject(pBuilding):spawnChildSceneObject("object/tangible/gcw/flip_banner_onpole_rebel.iff", -1145.8, 89.0, 4552.4, 0, 1, 0, 0, 0)

		BuildingObject(pBuilding):spawnChildSceneObject("object/tangible/gcw/flip_banner_onpole_rebel.iff", -1136.4, 78.1, 4584.3, 0, 1, 0, 0, 0)
		BuildingObject(pBuilding):spawnChildSceneObject("object/tangible/gcw/flip_banner_onpole_rebel.iff", -1231.9, 75.9, 4500.3, 0, 1, 0, 0, 0)

		BuildingObject(pBuilding):spawnChildSceneObject("object/tangible/camp/camp_pavilion_s1.iff", -1231.9, 75.9, 4480.3, 0, 1, 0, 0, 0)

		BuildingObject(pBuilding):spawnChildSceneObject("object/tangible/gcw/flip_banner_onpole_rebel.iff", 6.7, -12.0, 41.4, 479820, 1, 0, 0, 0)
		BuildingObject(pBuilding):spawnChildSceneObject("object/tangible/gcw/flip_banner_onpole_rebel.iff", 0.2, -12.0, 41.4, 479820, 1, 0, 0, 0)

		--rebels outside
		BuildingObject(pBuilding):spawnChildCreature("cis_battle_droid", 60, -1130.5, 77.2, 4511.5, 180, 0)
		BuildingObject(pBuilding):spawnChildCreature("cis_battle_droid", 60, -1136.9, 77.2, 4511.5, 180, 0)

		BuildingObject(pBuilding):spawnChildCreature("cis_battle_droid", 60, getRandomNumber(12) + -1142.5, 77.7, getRandomNumber(12) + 4586.5, getRandomNumber(180) + -90, 0)
		BuildingObject(pBuilding):spawnChildCreature("cis_battle_droid", 60, getRandomNumber(12) + -1142.5, 77.7, getRandomNumber(12) + 4586.5, getRandomNumber(180) + -90, 0)
		BuildingObject(pBuilding):spawnChildCreature("cis_battle_droid", 60, getRandomNumber(12) + -1142.5, 77.7, getRandomNumber(12) + 4586.5, getRandomNumber(180) + -90, 0)
		BuildingObject(pBuilding):spawnChildCreature("cis_battle_droid", 60, getRandomNumber(12) + -1142.5, 77.7, getRandomNumber(12) + 4586.5, getRandomNumber(180) + -90, 0)

		BuildingObject(pBuilding):spawnChildCreature("cis_battle_droid", 60, getRandomNumber(32) + -1150.0, 77.2, getRandomNumber(12) + 4533.5, getRandomNumber(180) + -90, 0)
		BuildingObject(pBuilding):spawnChildCreature("cis_battle_droid", 60, getRandomNumber(32) + -1150.0, 77.2, getRandomNumber(12) + 4533.5, getRandomNumber(180) + -90, 0)
		BuildingObject(pBuilding):spawnChildCreature("cis_battle_droid", 60, getRandomNumber(32) + -1150.0, 77.2, getRandomNumber(12) + 4533.5, getRandomNumber(180) + -90, 0)
		BuildingObject(pBuilding):spawnChildCreature("cis_battle_droid", 60, getRandomNumber(32) + -1150.0, 77.2, getRandomNumber(12) + 4533.5, getRandomNumber(180) + -90, 0)
		BuildingObject(pBuilding):spawnChildCreature("cis_battle_droid", 60, getRandomNumber(32) + -1150.0, 77.2, getRandomNumber(12) + 4533.5, getRandomNumber(180) + -90, 0)
		BuildingObject(pBuilding):spawnChildCreature("cis_sbd", 60, getRandomNumber(32) + -1150.0, 77.2, getRandomNumber(12) + 4533.5, getRandomNumber(180) + -90, 0)
		BuildingObject(pBuilding):spawnChildCreature("cis_sbd", 60, getRandomNumber(32) + -1150.0, 77.2, getRandomNumber(12) + 4533.5, getRandomNumber(180) + -90, 0)
		BuildingObject(pBuilding):spawnChildCreature("cis_sbd", 60, getRandomNumber(32) + -1150.0, 77.2, getRandomNumber(12) + 4533.5, getRandomNumber(180) + -90, 0)
		BuildingObject(pBuilding):spawnChildCreature("cis_sbd", 60, getRandomNumber(32) + -1150.0, 77.2, getRandomNumber(12) + 4533.5, getRandomNumber(180) + -90, 0)
		BuildingObject(pBuilding):spawnChildCreature("cis_sbd", 60, getRandomNumber(32) + -1150.0, 77.2, getRandomNumber(12) + 4533.5, getRandomNumber(180) + -90, 0)
		BuildingObject(pBuilding):spawnChildCreature("cis_sbd", 60, getRandomNumber(32) + -1150.0, 77.2, getRandomNumber(12) + 4533.5, getRandomNumber(180) + -90, 0)

		BuildingObject(pBuilding):spawnChildCreature("cis_battle_droid_captain", 60, -1232.0, 76.0, 4483.3, 180, 0)
		BuildingObject(pBuilding):spawnChildCreature("cis_battle_droid", 60, -1218.5, 76.0, 4486.9, 45, 0)
		BuildingObject(pBuilding):spawnChildCreature("cis_sbd", 60, -1228.4, 76.0, 4478.4, -75, 0)
		BuildingObject(pBuilding):spawnChildCreature("cis_battle_droid_captain", 60, -1234.5, 76.0, 4478.3, 75, 0)

		BuildingObject(pBuilding):spawnChildCreature("cis_battle_droid", 60, getRandomNumber(24) + -1244.0, 76.0, getRandomNumber(40) + 4465.5, getRandomNumber(360), 0)
		BuildingObject(pBuilding):spawnChildCreature("cis_battle_droid", 60, getRandomNumber(24) + -1244.0, 76.0, getRandomNumber(40) + 4465.5, getRandomNumber(360), 0)
		BuildingObject(pBuilding):spawnChildCreature("cis_battle_droid", 60, getRandomNumber(24) + -1244.0, 76.0, getRandomNumber(40) + 4465.5, getRandomNumber(360), 0)
		BuildingObject(pBuilding):spawnChildCreature("cis_battle_droid", 60, getRandomNumber(24) + -1244.0, 76.0, getRandomNumber(40) + 4465.5, getRandomNumber(360), 0)
		BuildingObject(pBuilding):spawnChildCreature("cis_battle_droid", 60, getRandomNumber(24) + -1244.0, 76.0, getRandomNumber(40) + 4465.5, getRandomNumber(360), 0)
		BuildingObject(pBuilding):spawnChildCreature("cis_battle_droid_captain", 60, getRandomNumber(24) + -1244.0, 76.0, getRandomNumber(40) + 4465.5, getRandomNumber(360), 0)
		BuildingObject(pBuilding):spawnChildCreature("cis_battle_droid_squad_leader", 60, getRandomNumber(24) + -1244.0, 76.0, getRandomNumber(40) + 4465.5, getRandomNumber(360), 0)
		BuildingObject(pBuilding):spawnChildCreature("cis_sbd", 60, getRandomNumber(24) + -1244.0, 76.0, getRandomNumber(40) + 4465.5, getRandomNumber(360), 0)
		BuildingObject(pBuilding):spawnChildCreature("cis_battle_droid", 60, -1185, 75.9, 4510, -135, 0)
		BuildingObject(pBuilding):spawnChildCreature("cis_battle_droid", 60, -1182, 75.9, 4513, -135, 0)
		BuildingObject(pBuilding):spawnChildCreature("cis_battle_droid", 60, -1179, 75.9, 4516, -135, 0)

		--rebels inside
		BuildingObject(pBuilding):spawnChildCreature("cis_battle_droid", 60, 0.0, 0.3, 8.9, 0, 479816)
		BuildingObject(pBuilding):spawnChildCreature("cis_battle_droid", 60, 0.0, 0.3, 6.4, 0,  479816)

		BuildingObject(pBuilding):spawnChildCreature("cis_battle_droid_captain", 60, -3.2, 0.3, -1.1, -90, 479817)

		BuildingObject(pBuilding):spawnChildCreature("cis_sbd", 60, 3.7, 0.3, -4.3, -90, 479818)

		BuildingObject(pBuilding):spawnChildCreature("cis_battle_droid", 60, 2.2, -12.0, 21.2, 90, 479819)
		BuildingObject(pBuilding):spawnChildCreature("cis_battle_droid", 60, 4.8, -12.0, 21.2, -90, 479819)

		BuildingObject(pBuilding):spawnChildCreature("cis_magnaguard", 60, 2.4, -12.0, 40.3, 0, 479820)
		BuildingObject(pBuilding):spawnChildCreature("cis_battle_droid_squad_leader", 60, 4.4, -12.0, 40.3, 0, 479820)
		BuildingObject(pBuilding):spawnChildCreature("cis_battle_droid", 60, -2.4, -12.0, 37.1, 90, 479820)
		BuildingObject(pBuilding):spawnChildCreature("cis_battle_droid", 60, -2.4, -12.0, 34.1, 90, 479820)
		BuildingObject(pBuilding):spawnChildCreature("cis_battle_droid", 60, -2.4, -12.0, 31.1, 90, 479820)
		BuildingObject(pBuilding):spawnChildCreature("cis_battle_droid", 60, -2.4, -12.0, 28.1, 90, 479820)
		BuildingObject(pBuilding):spawnChildCreature("cis_battle_droid", 60, 10.2, -12.0, 33.1, -90, 479820)
		BuildingObject(pBuilding):spawnChildCreature("cis_battle_droid", 60, 10.2, -12.0, 36.1, -90, 479820)
		BuildingObject(pBuilding):spawnChildCreature("cis_sbd", 60, 25.8, -12.0, 50.7, -155, 479820)
		BuildingObject(pBuilding):spawnChildCreature("cis_sbd", 60, 22.3, -12.0, 40.7, -55, 479820)
		BuildingObject(pBuilding):spawnChildCreature("cis_sbd", 60, 14.8, -12.0, 44.3, 55, 479820)
		BuildingObject(pBuilding):spawnChildCreature("cis_sbd", 60, 14.3, -12.0, 52.8, 180, 479820)
		BuildingObject(pBuilding):spawnChildCreature("cis_battle_droid_captain", 60, 10.1, -12.0, 47.2, -90, 479820)
		BuildingObject(pBuilding):spawnChildCreature("cis_battle_droid_captain", 60, -1.5, -12.0, 62.7, 90, 479820)
		BuildingObject(pBuilding):spawnChildCreature("cis_battle_droid_captain", 60, 1.9, -12.0, 61.2, 35, 479820)
		BuildingObject(pBuilding):spawnChildCreature("cis_battle_droid_captain", 60, 3.6, -12.0, 53.6, 180, 479820)
		BuildingObject(pBuilding):spawnChildCreature("cis_battle_droid", 60, -18.4, -12.0, 53.6, 180, 479820)
		BuildingObject(pBuilding):spawnChildCreature("cis_battle_droid", 60, -15.4, -12.0, 53.6, 180, 479820)
		BuildingObject(pBuilding):spawnChildCreature("cis_battle_droid", 60, -12.4, -12.0, 53.6, 180, 479820)
		BuildingObject(pBuilding):spawnChildCreature("cis_battle_droid", 60, -9.4, -12.0, 53.6, 180, 479820)
		BuildingObject(pBuilding):spawnChildCreature("cis_battle_droid_squad_leader", 60, -17.5, -12.0, 43.2, 40, 479820)

		BuildingObject(pBuilding):spawnChildCreature("cis_battle_droid", 60, -41.8, -20.0, 48.3, 180, 479821)
		BuildingObject(pBuilding):spawnChildCreature("cis_battle_droid", 60, -41.8, -20.0, 45.7, 0, 479821)

		BuildingObject(pBuilding):spawnChildCreature("cis_magnaguard", 60, -49.0, -20.0, 28.0, 90, 479822)
		BuildingObject(pBuilding):spawnChildCreature("cis_battle_droid_squad_leader", 60, -67.9, -20.0, 24.4, 175, 479822)
		BuildingObject(pBuilding):spawnChildCreature("cis_battle_droid_squad_leader", 60, -67.2, -20.0, 69.9, -24, 479822)

		BuildingObject(pBuilding):spawnChildCreature("cis_battle_droid_squad_leader", 60, -50.5, -20.0, 18.7, 24, 479823)
		BuildingObject(pBuilding):spawnChildCreature("cis_battle_droid_squad_leader", 60, -49.0, -20.0, 17.5, 64, 479823)
		BuildingObject(pBuilding):spawnChildCreature("cis_battle_droid_squad_leader", 60, -48.7, -20.0, 9.2, 120, 479823)
		BuildingObject(pBuilding):spawnChildCreature("cis_battle_droid_squad_leader", 60, -50.5, -20.0, 7.2, 160, 479823)
		BuildingObject(pBuilding):spawnChildCreature("cis_battle_droid_captain", 60, -60.7, -20.0, 11.7, -90, 479823)
		BuildingObject(pBuilding):spawnChildCreature("cis_battle_droid_captain", 1200, -60.7, -20.0, 14.0, -90, 479823)
		--BuildingObject(pBuilding):spawnChildCreature("cis_surface_marshall", 60, -59.1, -20.0, 6.9, 180, 479823)
		BuildingObject(pBuilding):spawnChildCreature("cis_sbd", 60, -62.6, -20.0, 6.9, 180, 479823)
		BuildingObject(pBuilding):spawnChildCreature("cis_surface_marshall", 60, -66.1, -20.0, 6.9, 180, 479823)
		BuildingObject(pBuilding):spawnChildCreature("cis_battle_droid_squad_leader", 60, -74.6, -20.0, 7.3, -145, 479823)
		BuildingObject(pBuilding):spawnChildCreature("cis_battle_droid_squad_leader", 60, -76.2, -20.0, 8.8, -125, 479823)
		BuildingObject(pBuilding):spawnChildCreature("cis_battle_droid", 60, -78.2, -20.0, 12.0, 90, 479823)
		BuildingObject(pBuilding):spawnChildCreature("cis_battle_droid", 60, -78.2, -20.0, 14.4, 90, 479823)
		BuildingObject(pBuilding):spawnChildCreature("cis_battle_droid", 60, -75.6, -20.0, 17.9, -45, 479823)
		--BuildingObject(pBuilding):spawnChildCreature("cis_surface_marshall", 60, -59.1, -20.0, 19.0, 0, 479823)
		BuildingObject(pBuilding):spawnChildCreature("cis_sbd", 60, -62.6, -20.0, 19.0, 0, 479823)
		BuildingObject(pBuilding):spawnChildCreature("cis_surface_marshall", 60, -66.1, -20.0, 19.0, 0, 479823)

		BuildingObject(pBuilding):spawnChildCreature("cis_battle_droid", 60, -77.2, -20.0, 74.3, 45, 479824)
		BuildingObject(pBuilding):spawnChildCreature("cis_battle_droid", 60, -74.4, -20.0, 86.7, 135, 479824)
		BuildingObject(pBuilding):spawnChildCreature("cis_battle_droid", 60, -56.7, -20.0, 75.2, 180, 479824)
		BuildingObject(pBuilding):spawnChildCreature("cis_sbd", 60, -69.2, -20.0, 77.3, -90, 479824)

		BuildingObject(pBuilding):spawnChildCreature("cis_sbd", 60, 35.7, -12.0, 40.6, 0, 479825)
		BuildingObject(pBuilding):spawnChildCreature("cis_sbd", 60, 35.7, -12.0, 66.2, 180, 479825)
		BuildingObject(pBuilding):spawnChildCreature("cis_sbd", 60, 35.7, -12.0, 78.2, 180, 479825)
		BuildingObject(pBuilding):spawnChildCreature("cis_battle_droid", 60, 35.7, -12.0, 87.9, 180, 479825)

		BuildingObject(pBuilding):spawnChildCreature("cis_battle_droid", 60, 49.3, -12.0, 87.8, 180, 479826)
		BuildingObject(pBuilding):spawnChildCreature("cis_battle_droid", 60, 56.7, -12.0, 87.8, 180, 479826)
		BuildingObject(pBuilding):spawnChildCreature("cis_battle_droid", 60, 63.3, -12.0, 87.8, 180, 479826)
		BuildingObject(pBuilding):spawnChildCreature("cis_battle_droid", 60, 70.0, -12.0, 87.8, 180, 479826)
		BuildingObject(pBuilding):spawnChildCreature("cis_battle_droid", 60, 49.3, -12.0, 78.4, 0, 479826)
		BuildingObject(pBuilding):spawnChildCreature("cis_battle_droid", 60, 56.7, -12.0, 78.4, 0, 479826)
		BuildingObject(pBuilding):spawnChildCreature("cis_battle_droid", 60, 63.3, -12.0, 78.4, 0, 479826)
		BuildingObject(pBuilding):spawnChildCreature("cis_battle_droid", 60, 70.0, -12.0, 78.4, 0, 479826)

		BuildingObject(pBuilding):spawnChildCreature("cis_battle_droid_captain", 60, 50.4, -12.0, 62.6, 82, 479827)
		BuildingObject(pBuilding):spawnChildCreature("cis_battle_droid_captain", 60, 50.4, -12.0, 55.8, 122, 479827)

		BuildingObject(pBuilding):spawnChildCreature("cis_battle_droid", 60, 36.4, -20.0, 111.8, 180, 479828)
		BuildingObject(pBuilding):spawnChildCreature("cis_battle_droid", 60, 34.6, -20.0, 111.8, 180, 479828)

		BuildingObject(pBuilding):spawnChildCreature("cis_battle_droid", 60, getRandomNumber(15) + 16.5, -12.0, getRandomNumber(29) + -4.2, getRandomNumber(360), 479829)
		BuildingObject(pBuilding):spawnChildCreature("cis_battle_droid", 60, getRandomNumber(15) + 16.5, -12.0, getRandomNumber(29) + -4.2, getRandomNumber(360), 479829)
		BuildingObject(pBuilding):spawnChildCreature("cis_battle_droid", 60, getRandomNumber(15) + 16.5, -12.0, getRandomNumber(29) + -4.2, getRandomNumber(360), 479829)
		BuildingObject(pBuilding):spawnChildCreature("cis_battle_droid", 60, getRandomNumber(15) + 16.5, -12.0, getRandomNumber(29) + -4.2, getRandomNumber(360), 479829)
		BuildingObject(pBuilding):spawnChildCreature("cis_battle_droid", 60, getRandomNumber(15) + 16.5, -12.0, getRandomNumber(29) + -4.2, getRandomNumber(360), 479829)
		BuildingObject(pBuilding):spawnChildCreature("cis_battle_droid_squad_leader", 60, getRandomNumber(15) + 16.5, -12.0, getRandomNumber(29) + -4.2, getRandomNumber(360), 479829)
		BuildingObject(pBuilding):spawnChildCreature("cis_battle_droid_squad_leader", 60, getRandomNumber(15) + 16.5, -12.0, getRandomNumber(29) + -4.2, getRandomNumber(360), 479829)
		BuildingObject(pBuilding):spawnChildCreature("cis_sbd", 60, getRandomNumber(15) + 16.5, -12.0, getRandomNumber(29) + -4.2, getRandomNumber(360), 479829)
		BuildingObject(pBuilding):spawnChildCreature("cis_battle_droid_squad_leader", 60, getRandomNumber(15) + 16.5, -12.0, getRandomNumber(29) + -4.2, getRandomNumber(360), 479829)
		BuildingObject(pBuilding):spawnChildCreature("cis_battle_droid_squad_leader", 60, getRandomNumber(15) + 16.5, -12.0, getRandomNumber(29) + -4.2, getRandomNumber(360), 479829)
		BuildingObject(pBuilding):spawnChildCreature("cis_sbd", 60, getRandomNumber(15) + 16.5, -12.0, getRandomNumber(29) + -4.2, getRandomNumber(360), 479829)
		BuildingObject(pBuilding):spawnChildCreature("cis_battle_droid_captain", 60, getRandomNumber(15) + 16.5, -12.0, getRandomNumber(29) + -4.2, getRandomNumber(360), 479829)
		BuildingObject(pBuilding):spawnChildCreature("cis_surface_marshall", 60, 61.6, -12.0, 16.3, 90, 479829)
		BuildingObject(pBuilding):spawnChildCreature("cis_surface_marshall", 60, 61.6, -12.0, 5.5, 90, 479829)

		BuildingObject(pBuilding):spawnChildCreature("cis_battle_droid", 60, -5.1, -20.0, 3.3, 90, 479830)

		BuildingObject(pBuilding):spawnChildCreature("cis_battle_droid", 60, getRandomNumber(14) + -23.7, -20.0, getRandomNumber(13) + -4.2, getRandomNumber(360), 479831)
		BuildingObject(pBuilding):spawnChildCreature("cis_battle_droid", 60, getRandomNumber(14) + -23.7, -20.0, getRandomNumber(13) + -4.2, getRandomNumber(360), 479831)
		BuildingObject(pBuilding):spawnChildCreature("cis_battle_droid", 60, getRandomNumber(14) + -23.7, -20.0, getRandomNumber(13) + -4.2, getRandomNumber(360), 479831)
		BuildingObject(pBuilding):spawnChildCreature("cis_battle_droid", 60, getRandomNumber(14) + -23.7, -20.0, getRandomNumber(13) + -4.2, getRandomNumber(360), 479831)
		BuildingObject(pBuilding):spawnChildCreature("cis_sbd", 60, getRandomNumber(14) + -23.7, -20.0, getRandomNumber(13) + -4.2, getRandomNumber(360), 479831)
		BuildingObject(pBuilding):spawnChildCreature("cis_sbd", 60, getRandomNumber(14) + -23.7, -20.0, getRandomNumber(13) + -4.2, getRandomNumber(360), 479831)
		BuildingObject(pBuilding):spawnChildCreature("cis_sbd", 60, getRandomNumber(14) + -23.7, -20.0, getRandomNumber(13) + -4.2, getRandomNumber(360), 479831)
		BuildingObject(pBuilding):spawnChildCreature("cis_sbd", 60, getRandomNumber(14) + -23.7, -20.0, getRandomNumber(13) + -4.2, getRandomNumber(360), 479831)

		BuildingObject(pBuilding):spawnChildCreature("cis_battle_droid", 60, -29.8, -20.0, 21.0, 0, 479833)
		BuildingObject(pBuilding):spawnChildCreature("cis_battle_droid", 60, -29.8, -20.0, 36.0, 0, 479833)
		BuildingObject(pBuilding):spawnChildCreature("cis_battle_droid", 60, -31.3, -20.0, 37.0, 180, 479833)
		BuildingObject(pBuilding):spawnChildCreature("cis_battle_droid", 60, -31.3, -20.0, 44.0, 180, 479833)
		BuildingObject(pBuilding):spawnChildCreature("cis_battle_droid", 60, -29.8, -20.0, 57.0, 0, 479833)
		BuildingObject(pBuilding):spawnChildCreature("cis_battle_droid", 60, -31.3, -20.0, 59.0, 180, 479833)
		BuildingObject(pBuilding):spawnChildCreature("cis_battle_droid", 60, -31.3, -20.0, 71.0, 180, 479833)
		BuildingObject(pBuilding):spawnChildCreature("cis_battle_droid", 60, -29.8, -20.0, 81.0, 0, 479833)
		BuildingObject(pBuilding):spawnChildCreature("cis_battle_droid", 60, -31.3, -20.0, 82.0, 180, 479833)

	BuildingObject(pBuilding):spawnChildCreature("cis_battle_droid", 60, -8.0, -20.0, 102.1, 110, 479834)
	BuildingObject(pBuilding):spawnChildCreature("cis_battle_droid", 60, -37.4, -20.0, 99.9, -55, 479834)

		BuildingObject(pBuilding):spawnChildCreature("cis_battle_droid_captain", 60, getRandomNumber(12) + -9.5, -20.0, getRandomNumber(12) + 65.3, getRandomNumber(360), 479836)
		BuildingObject(pBuilding):spawnChildCreature("cis_battle_droid_captain", 60, getRandomNumber(12) + -9.5, -20.0, getRandomNumber(12) + 65.3, getRandomNumber(360), 479836)
		BuildingObject(pBuilding):spawnChildCreature("cis_battle_droid_captain", 60, getRandomNumber(12) + -9.5, -20.0, getRandomNumber(12) + 65.3, getRandomNumber(360), 479836)
		BuildingObject(pBuilding):spawnChildCreature("cis_battle_droid_captain", 60, getRandomNumber(12) + -9.5, -20.0, getRandomNumber(12) + 65.3, getRandomNumber(360), 479836)
end
