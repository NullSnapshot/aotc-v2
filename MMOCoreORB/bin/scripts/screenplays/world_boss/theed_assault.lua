-- Original Screenplay Written by: Alpha 2024

theedAssault = ScreenPlay:new {
	scriptName = "theedAssault",
	planetName = "naboo",
	x = -5494,
	z = 6,
	y = 4396,

	bossShapes = { 
		"anakin_skywalker",
		"general_grievous",
	},
	
	mobiles = {
		{"rep_jedi_knight", 0, 5.1291, 27, -152.921, 351, 1688865, ""},
		{"rep_jedi_knight", 0, -4.59556, 27, -152.969, 4, 1688865, ""},
		{"fbase_rebel_colonel_extreme", 0, 22.8651, 23.0018, -37.8767, 272, 1688861, ""},
		{"fbase_rebel_colonel_extreme", 0, 22.3748, 23.0017, -26.8767, 321, 1688861, ""},
		
		{"clonetrooper_501", 0, -29.8655, 27, -29.999, 89, 1688862, ""},
		{"clonetrooper_501", 0, -29.8248, 27, -34.9475, 83, 1688862, ""},
		{"fbase_scout_trooper_extreme", 0, -30.7867, 27, -50.0375, 14, 1688862, ""},
		{"restuss_padawan", 0, -31.7867, 27, -50.0375, 14, 1688862, ""},
		{"fbase_stormtrooper_bombardier_extreme", 0, -33.2053, 27, -48.1177, 292, 1688862, ""},
		{"clonetrooper_501", 0, -34.2518, 27, -47.3475, 4, 1688862, ""},
		{"fbase_elite_dark_trooper_extreme", 0, -31.427, 27, -42.9922, 25, 1688862, ""},
		{"clonetrooper_captain_501", 0, -30.8868, 27, -32.7659, 89, 1688862, ""},
		{"fbase_elite_dark_trooper_extreme", 0, -32.8142, 27, -55.4473, 177, 1688863, ""},
		{"rep_jedi_master", 0, -32.4036, 27, -62.0132, 0, 1688863, ""} ,
		{"fbase_at_xt", 0, 0.523196, 27, -112.683, 356, 1688865, ""},
		{"rep_jedi_master", 0, 14.9115, 27, -94.1911, 84, 1688865, ""},
		{"restuss_padawan", 0, 13.17511, 27, -95.8499, 358, 1688865, ""},
		{"clonetrooper_501", 0, 3.17511, 27, -95.8499, 358, 1688865, ""},
		{"clonetrooper_501", 0, -0.260417, 27, -96.3509, 342, 1688865, ""},
		{"clonetrooper_501", 0, -0.863287, 27, -96.3191, 262, 1688865, ""},
		{"clonetrooper_501", 0, -5.1014, 27, -96.475, 16, 1688865, ""},
		{"clonetrooper_501", 0, -3.99946, 27, -93.7798, 17, 1688865, ""},
		{"clonetrooper_501", 0, -3.81736, 27, -93.2286, 60, 1688865, ""},
		{"clonetrooper_501", 0, 1.45351, 27, -93.5418, 2, 1688865, ""},
		{"clonetrooper_501", 0, 3.09447, 27, -94.7972, 121, 1688865, ""},
		{"clonetrooper_501", 0, 5.36208, 27, -92.3791, 333, 1688865, ""},
		{"fbase_elite_dark_trooper_extreme", 0, -8.8618, 27, -70.4181, 271, 1688864, ""},
		{"fbase_elite_dark_trooper_extreme", 0, -8.27054, 27, -68.1232, 2, 1688864, ""},
		{"fbase_elite_dark_trooper_extreme", 0, -14.0798, 27, -68.881, 271, 1688863, ""},
		{"fbase_elite_dark_trooper_extreme", 0, -6.74019, 27, -119.2, 9, 1688865, ""},
		{"fbase_stormtrooper_sniper_extreme", 0, 6.59002, 27, -119.879, 62, 1688865, ""},
		{"fbase_imperial_army_captain_extreme", 0, -2.63403, 27, -157.143, 11, 1688865, ""},
		
		{"fbase_hailfire", 0, -5505.4, 6, 4399.57, 186, 0, ""},
		{"fbase_hailfire", 0, -5487.28, 6, 4397.16, 180, 0, ""},
		
		{"cis_droideka", 0, -5510.19, 6, 4315.19, 177, 0, ""},
		{"cis_droideka", 0, -5482.15, 6, 4316.36, 179, 0, ""},
		{"cis_droideka", 0, -5487.02, 6, 4274.81, 184, 0, ""},
		{"cis_droideka", 0, -5504.51, 6, 4275.68, 164, 0, ""},

		{"fbase_rebel_colonel_extreme", 0, -5507.13, 6, 4315.51, 181, 0, ""},
		{"cis_battle_droid_geo", 0, -5503.37, 6, 4315.94, 183, 0, ""},
		{"cis_battle_droid_geo", 0, -5500, 6, 4315.75, 182, 0, ""},
		{"cis_battle_droid_geo", 0, -5496.87, 6, 4315.79, 183, 0, ""},
		{"cis_battle_droid_geo", 0, -5493.19, 6, 4315.58, 184, 0, ""},
		{"cis_battle_droid_geo", 0, -5489.38, 6, 4315.62, 184, 0, ""},
		{"cis_battle_droid_geo", 0, -5485.22, 6, 4315.5, 181, 0, ""},
		{"cis_battle_droid_geo", 0, -5507.13, 6, 4322.51, 181, 0, ""},
		{"cis_battle_droid_geo", 0, -5503.37, 6, 4322.94, 183, 0, ""},
		{"cis_battle_droid_geo", 0, -5500, 6, 4322.75, 182, 0, ""},
		{"cis_battle_droid_geo", 0, -5496.87, 6, 4322.79, 183, 0, ""},
		{"cis_battle_droid_geo", 0, -5493.19, 6, 4322.58, 184, 0, ""},
		{"cis_battle_droid_geo", 0, -5489.38, 6, 4322.62, 184, 0, ""},
		{"cis_battle_droid_geo", 0, -5485.22, 6, 4322.5, 181, 0, ""},
		{"cis_battle_droid_geo", 0, -5507.13, 6, 4329.51, 181, 0, ""},
		{"cis_battle_droid_geo", 0, -5503.37, 6, 4329.94, 183, 0, ""},
		{"cis_battle_droid_geo", 0, -5500, 6, 4329.75, 182, 0, ""},
		{"cis_battle_droid_geo", 0, -5496.87, 6, 4329.79, 183, 0, ""},
		{"cis_battle_droid_geo", 0, -5493.19, 6, 4329.58, 184, 0, ""},
		{"cis_battle_droid_geo", 0, -5489.38, 6, 4329.62, 184, 0, ""},
		{"cis_battle_droid_geo", 0, -5485.22, 6, 4329.5, 181, 0, ""},
		{"cis_battle_droid_geo", 0, -5507.13, 6, 4336.51, 181, 0, ""},
		{"cis_battle_droid_geo", 0, -5503.37, 6, 4336.94, 183, 0, ""},
		{"cis_battle_droid_geo", 0, -5500, 6, 4336.75, 182, 0, ""},
		{"cis_battle_droid_geo", 0, -5496.87, 6, 4336.79, 183, 0, ""},
		{"cis_battle_droid_geo", 0, -5493.19, 6, 4336.58, 184, 0, ""},
		{"cis_battle_droid_geo", 0, -5489.38, 6, 4336.62, 184, 0, ""},
		{"fbase_rebel_squad_leader_extreme", 0, -5485.22, 6, 4336.5, 181, 0, ""},
		
		{"fbase_rebel_colonel_extreme", 0, -5507.13, 6, 4241.51, 181, 0, ""},
		{"cis_battle_droid_geo", 0, -5503.37, 6, 4241.94, 183, 0, ""},
		{"cis_battle_droid_geo", 0, -5500, 6, 4241.75, 182, 0, ""},
		{"cis_battle_droid_geo", 0, -5496.87, 6, 4241.79, 183, 0, ""},
		{"cis_battle_droid_geo", 0, -5493.19, 6, 4241.58, 184, 0, ""},
		{"cis_battle_droid_geo", 0, -5489.38, 6, 4241.62, 184, 0, ""},
		{"cis_battle_droid_geo", 0, -5485.22, 6, 4241.5, 181, 0, ""},
		{"cis_battle_droid_geo", 0, -5507.13, 6, 4248.51, 181, 0, ""},
		{"cis_battle_droid_geo", 0, -5503.37, 6, 4248.94, 183, 0, ""},
		{"cis_battle_droid_geo", 0, -5500, 6, 4248.75, 182, 0, ""},
		{"cis_battle_droid_geo", 0, -5496.87, 6, 4248.79, 183, 0, ""},
		{"cis_battle_droid_geo", 0, -5493.19, 6, 4248.58, 184, 0, ""},
		{"cis_battle_droid_geo", 0, -5489.38, 6, 4248.62, 184, 0, ""},
		{"cis_battle_droid_geo", 0, -5485.22, 6, 4248.5, 181, 0, ""},
		{"cis_battle_droid_geo", 0, -5507.13, 6, 4255.51, 181, 0, ""},
		{"cis_battle_droid_geo", 0, -5503.37, 6, 4255.94, 183, 0, ""},
		{"cis_battle_droid_geo", 0, -5500, 6, 4255.75, 182, 0, ""},
		{"cis_battle_droid_geo", 0, -5496.87, 6, 4255.79, 183, 0, ""},
		{"cis_battle_droid_geo", 0, -5493.19, 6, 4255.58, 184, 0, ""},
		{"cis_battle_droid_geo", 0, -5489.38, 6, 4255.62, 184, 0, ""},
		{"cis_battle_droid_geo", 0, -5485.22, 6, 4255.5, 181, 0, ""},
		{"cis_battle_droid_geo", 0, -5507.13, 6, 4262.51, 181, 0, ""},
		{"cis_battle_droid_geo", 0, -5503.37, 6, 4262.94, 183, 0, ""},
		{"cis_battle_droid_geo", 0, -5500, 6, 4262.75, 182, 0, ""},
		{"cis_battle_droid_geo", 0, -5496.87, 6, 4262.79, 183, 0, ""},
		{"cis_battle_droid_geo", 0, -5493.19, 6, 4262.58, 184, 0, ""},
		{"cis_battle_droid_geo", 0, -5489.38, 6, 4262.62, 184, 0, ""},
		{"fbase_rebel_squad_leader_extreme", 0, -5485.22, 6, 4262.5, 181, 0, ""},
		
		{"restuss_magnaguard", 0, -5482.77, 10.0596, 4421.64, 178, 0, ""},
		{"restuss_magnaguard", 0, -5509.08, 10.0107, 4422.11, 193, 0, ""},
		{"fbase_rebel_heavy_trooper_hard", 0, -5492.37, 10.0742, 4429.67, 147, 0, ""},
		{"fbase_rebel_heavy_trooper_hard", 0, -5498.51, 10.0742, 4429.51, 241, 0, ""},
		{"fbase_rebel_heavy_trooper_hard", 0, -5506.13, 10.0742, 4428.73, 191, 0, ""},
		{"fbase_rebel_heavy_trooper_hard", 0, -5486.67, 10.0742, 4429.62, 172, 0, ""},
		{"fbase_rebel_heavy_trooper_hard", 0, -5489.26, 13.999, 4448.51, 183, 0, ""},
		{"fbase_rebel_heavy_trooper_hard", 0, -5498.61, 14, 4447, 182, 0, ""},
		{"fbase_rebel_heavy_trooper_hard", 0, -5507.26, 13.999, 4448.55, 161, 0, ""},
		{"cis_magnaguard", 0, -0.574749, 12.001, 71.0476, 359, 1688852, ""},
		{"cis_magnaguard", 0, 3.94894, 12.0012, 72.1263, 327, 1688852, ""},
		{"cis_magnaguard", 0, -6.21701, 12.0006, 71.3887, 66, 1688852, ""},
		{"fbase_hailfire", 0, 0.117766, 12.0044, 23.5442, 354, 1688859, ""},
		{"cis_battle_droid", 0, 7.19664, 18.9998, -1.50838, 357, 1688859, ""},
		{"cis_battle_droid", 0, 2.19664, 18.9998, -1.50838, 357, 1688859, ""},
		{"cis_battle_droid", 0, -8.96738, 18.9999, -1.40888, 355, 1688859, ""},
		{"cis_battle_droid", 0, -9.99891, 12.0074, 18.7153, 359, 1688859, ""},
		{"cis_battle_droid", 0, -18.1736, 12.0048, 22.4007, 291, 1688859, ""},
		{"cis_battle_droid", 0, -12.2398, 12.0037, 24.0136, 76, 1688859, ""},
		{"fbase_rebel_squad_leader_extreme", 0, 3.51909, 12, 37.8076, 0, 1688856, ""},
		{"cis_battle_droid", 0, 3.41526, 12, 39.8419, 353, 1688856, ""},
		{"cis_battle_droid", 0, -7.42626, 12, 44.9125, 288, 1688856, ""},
		{"cis_battle_droid", 0, -4.20159, 12, 48.2176, 58, 1688856, ""},
		{"cis_battle_droid", 0, -4.20159, 12, 48.2176, 58, 1688856, ""},
		{"cis_battle_droid", 0, 8.26811, 12, 46.07, 100, 1688856, ""},
		{"cis_battle_droid", 0, 6.07861, 12, 50.0641, 319, 1688856, ""},
		{"cis_battle_droid", 0, 1.33021, 11.9997, 55.34, 319, 1688856, ""},
		{"cis_battle_droid", 0, -3.34187, 11.9999, 63.8852, 334, 1688855, ""},
		
	}
}

registerScreenPlay("theedAssault", false)

function theedAssault:start()
	createEvent(2 * 1000, self.scriptName, "startEncounter", nil, "")
end

function theedAssault:stop()

end

function theedAssault:startEncounter()
	broadcastGalaxy("all", "A major battle has begun in the city of Theed. The palace is under attack!")

	local pBoss = spawnMobile(self.planetName, self.bossShapes[1], 0, 0.175098, 27, -157.296, 359, 1688865)
	writeData("theedAssault:bossID", SceneObject(pBoss):getObjectID())
	writeData("theedAssault:bossState", 1)

	local pBoss2 = spawnMobile(self.planetName, self.bossShapes[2], 0, 30.849, 23.0012, -32.5834, 268, 1688861)
	writeData("theedAssault:boss2ID", SceneObject(pBoss2):getObjectID())
	writeData("theedAssault:boss2State", 1)
	
	self:spawnMobiles()
	self:spawnSceneObjects()
	self:setupMusic(pBoss)
	
	createEvent(5 * 1000, self.scriptName, "doHealingPulse", nil, "")

end

function theedAssault:setupMusic(pBoss)
	if (pBoss == nil) then
		return
	end

	local playerTable = SceneObject(pBoss):getPlayersInRange(1024)
	local musicTemplate = ""
	local bossState = 1
			
	if (bossState == 1) then
		musicTemplate = "sound/visions2_01.snd"
	end
	

	if (#playerTable > 0) then
		for i = 0, #playerTable, 1 do
			local pPlayer = playerTable[i]

			if (pPlayer ~= nil and musicTemplate ~= nil) then
				CreatureObject(pPlayer):playMusicMessage(musicTemplate)
			end
		end
	end
end

function theedAssault:spawnMobiles()
	local mobiles = self.mobiles
	for i = 1, #mobiles, 1 do
		local mob = mobiles[i]
		-- {template, respawn, x, z, y, direction, cell, mood}
		local pMobile = spawnMobile(self.planetName, mob[1], mob[2], mob[3], mob[4], mob[5], mob[6], mob[7])

		if (pMobile ~= nil) then
			if mob[8] ~= "" then
				CreatureObject(pMobile):setMoodString(mob[8])
			end
			AiAgent(pMobile):addObjectFlag(AI_STATIC)
		end
	end
end

function theedAssault:spawnSceneObjects()
	local turretData = { template = "object/installation/faction_perk/turret/tower_lg.iff", x = -5475, z = 6, y = 4397 }
	local pTurret = spawnSceneObject("naboo", turretData.template, turretData.x, turretData.z, turretData.y, 0, 0, 0, -1, 0)
	if pTurret ~= nil then
		local turret = TangibleObject(pTurret)
		turret:setFaction(FACTIONREBEL)
		turret:setPvpStatusBitmask(1)
		
		createObserver(OBJECTDESTRUCTION, "theedAssault", "notifyTurretDestroyed", pTurret)
	end
	
	turretData = { template = "object/installation/faction_perk/turret/tower_lg.iff", x = -5517, z = 6, y = 4397 }
	pTurret = spawnSceneObject("naboo", turretData.template, turretData.x, turretData.z, turretData.y, 0, 0, 0, -1, 0)
	if pTurret ~= nil then
		local turret = TangibleObject(pTurret)
		turret:setFaction(FACTIONREBEL)
		turret:setPvpStatusBitmask(1)
		
		createObserver(OBJECTDESTRUCTION, "theedAssault", "notifyTurretDestroyed", pTurret)
	end
	
	turretData = { template = "object/installation/turret/turret_dish_sm.iff", x = -3.4, z = 27, y = -135 }
	pTurret = spawnSceneObject("naboo", turretData.template, turretData.x, turretData.z, turretData.y, 1688865, 180, 0, -1, 0)
	if pTurret ~= nil then
		local turret = TangibleObject(pTurret)
		turret:setFaction(FACTIONIMPERIAL)
		turret:setPvpStatusBitmask(1)
		
		createObserver(OBJECTDESTRUCTION, "theedAssault", "notifyTurretDestroyed", pTurret)
	end
	
	turretData = { template = "object/installation/turret/turret_dish_sm.iff", x = 5.1, z = 27, y = -135 }
	pTurret = spawnSceneObject("naboo", turretData.template, turretData.x, turretData.z, turretData.y, 1688865, 180, 0, -1, 0)
	if pTurret ~= nil then
		local turret = TangibleObject(pTurret)
		turret:setFaction(FACTIONIMPERIAL)
		turret:setPvpStatusBitmask(1)
		
		createObserver(OBJECTDESTRUCTION, "theedAssault", "notifyTurretDestroyed", pTurret)
	end

	spawnSceneObject("naboo", "object/static/vehicle/static_speeder_bike.iff", -5494.7, 6, 4399, 0, 54 )
	spawnSceneObject("naboo", "object/static/vehicle/static_speeder_bike.iff", -5499, 6, 4396, 0, 54 )
end

function theedAssault:notifyTurretDestroyed(pTurret, pPlayer)
	SceneObject(pTurret):destroyObjectFromWorld()
	CreatureObject(pPlayer):clearCombatState(1)
	return 0
end

---- Healing Mechanic
function theedAssault:doHealingPulse()
	local pBoss = getSceneObject(readData("theedAssault:bossID"))
	local pBoss2 = getSceneObject(readData("theedAssault:boss2ID"))
	local bossState = readData("theedAssault:bossState")
	local boss2State = readData("theedAssault:boss2State")
	
	for i = 0, 6, 3 do
		if (CreatureObject(pBoss):getHAM(i) <= CreatureObject(pBoss):getMaxHAM(i) * 0.1 and CreatureObject(pBoss):getHAM(i) >= CreatureObject(pBoss):getMaxHAM(i) * 0.01) then
			broadcastGalaxy("all", "General Skywalker has been forced to retreat! The Separatists are victorious.")
			spatialMoodChat(pBoss, "A minor set back!", "18", "80") --shouts confidently
			spatialMoodChat(pBoss2, "Exterminate the Jedi scum!", "18", "80") --shouts confidently
			bossState = 0
			self:giveAwards(pBoss2, "CIS")
			self:giveAwards(pBoss2, "all")
			
			spawnMobile(self.planetName, "cis_droideka", 0, 3.175098, 27, -30.296, 359, 1688860)
			spawnMobile(self.planetName, "fbase_rebel_squad_leader_extreme", 0, 1.975098, 27, -32.296, 86, 1688860)
			spawnMobile(self.planetName, "fbase_rebel_squad_leader_extreme", 0, 1.175098, 27, -29.296, 86, 1688860)
			spawnMobile(self.planetName, "fbase_rebel_squad_leader_extreme", 0, 1.175098, 27, -27.296, 86, 1688860)
			spawnMobile(self.planetName, "fbase_rebel_squad_leader_extreme", 0, 1.175098, 27, -25.296, 86, 1688860)
			spawnMobile(self.planetName, "fbase_rebel_squad_leader_extreme", 0, 1.175098, 27, -23.296, 86, 1688860)
			
			spawnMobile(self.planetName, "fbase_rebel_colonel_extreme", 0, 27.775098, 27, -23.296, 359, 1688861)
			spawnMobile(self.planetName, "fbase_rebel_colonel_extreme", 0, 27.175098, 27, -43.296, 359, 1688861)
			
			spawnMobile(self.planetName, "cis_droideka", 0, 13.175098, 27, -26.296, 86, 1688860)
			spawnMobile(self.planetName, "fbase_rebel_heavy_trooper_extreme", 0, 13.175098, 27, -28.296, 86, 1688860)
			spawnMobile(self.planetName, "fbase_rebel_heavy_trooper_extreme", 0, 13.175098, 27, -30.296, 86, 1688860)
			spawnMobile(self.planetName, "fbase_rebel_heavy_trooper_extreme", 0, 13.175098, 27, -32.296, 86, 1688860)
			spawnMobile(self.planetName, "fbase_rebel_heavy_trooper_extreme", 0, 13.175098, 27, -34.296, 86, 1688860)
			
			createEvent(2 * 1000, self.scriptName, "retreatMechanic", pBoss, "")
			createEvent(3 * 1000, self.scriptName, "deleteMechanic", pBoss, "")
		end
	end
	
	for i = 0, 6, 3 do
		if (CreatureObject(pBoss2):getHAM(i) <= CreatureObject(pBoss2):getMaxHAM(i) * 0.1 and CreatureObject(pBoss2):getHAM(i) >= CreatureObject(pBoss2):getMaxHAM(i) * 0.01) then
			broadcastGalaxy("all", "General Grievous has been forced to retreat! The Republic is victorious.")
			spatialMoodChat(pBoss2, "Impossible!", "18", "80") --shouts confidently
			spatialMoodChat(pBoss, "Reinforcements incoming! Push forward!", "18", "80") --shouts confidently
			boss2State = 0
			self:giveAwards(pBoss, "Republic")
			self:giveAwards(pBoss, "all")
			
			spawnMobile(self.planetName, "fbase_elite_dark_trooper_extreme", 0, 12.175098, 27, -147.296, 90, 1688865)
			spawnMobile(self.planetName, "fbase_elite_dark_trooper_extreme", 0, 13.175098, 27, -120.296, 90, 1688865)
			spawnMobile(self.planetName, "fbase_elite_dark_trooper_extreme", 0, 13.175098, 27, -93.296, 90, 1688865)
			spawnMobile(self.planetName, "fbase_elite_dark_trooper_extreme", 0, -11.175098, 27, -93.296, 90, 1688865)
			spawnMobile(self.planetName, "fbase_elite_dark_trooper_extreme", 0, -12.175098, 27, -120.296, 90, 1688865)
			spawnMobile(self.planetName, "fbase_elite_dark_trooper_extreme", 0, -12.175098, 27, -147.296, 90, 1688865)
			
			spawnMobile(self.planetName, "rep_jedi_master", 0, -2.775098, 27, -123.296, 359, 1688865)
			spawnMobile(self.planetName, "rep_jedi_master", 0, 4.175098, 27, -147.296, 359, 1688865)
			
			spawnMobile(self.planetName, "fbase_stormtrooper_bombardier_extreme", 0, -2.175098, 27, -127.296, 180, 1688865)
			spawnMobile(self.planetName, "fbase_stormtrooper_bombardier_extreme", 0, -1.175098, 27, -127.296, 180, 1688865)
			spawnMobile(self.planetName, "fbase_stormtrooper_bombardier_extreme", 0, 0.175098, 27, -127.296, 180, 1688865)
			spawnMobile(self.planetName, "fbase_stormtrooper_bombardier_extreme", 0, 1.175098, 27, -127.296, 180, 1688865)
			spawnMobile(self.planetName, "fbase_stormtrooper_bombardier_extreme", 0, 2.175098, 27, -127.296, 180, 1688865)
			
			createEvent(2 * 1000, self.scriptName, "retreatMechanic", pBoss2, "")
			createEvent(3 * 1000, self.scriptName, "deleteMechanic", pBoss2, "")
		end
	end
	
	if (bossState == 1 and boss2State == 1) then
		createEvent(2 * 1000, self.scriptName, "doHealingPulse", nil, "")
	end
end

---- Retreat Mechanics
function theedAssault:retreatMechanic(pBoss)
	CreatureObject(pBoss):playEffect("clienteffect/entertainer_smoke_bomb_level_1.cef", "")
end

function theedAssault:deleteMechanic(pBoss)
	SceneObject(pBoss):destroyObjectFromWorld()
end

---- Utilities
-- Cleanup
function theedAssault:destroyObject(pObject)
	if (pObject == nil) then
		return
	end

	if (SceneObject(pObject):isAiAgent()) then
		CreatureObject(pObject):setPvpStatusBitmask(0)
		forcePeace(pObject)
	end
	SceneObject(pObject):destroyObjectFromWorld()
end

-- Cleanup
function theedAssault:teleportPlayer(pCreature)
	if (pCreature == nil) then
		return
	end
end

function theedAssault:giveAwards(pBoss, faction)
	if (pBoss == nil) then
		return
	end

	local playerTable = SceneObject(pBoss):getPlayersInRange(500)
	local musicTemplate = "sound/visions_05.snd"

	local rngloot = 0
	
	if (#playerTable > 0) then
		for i = 0, #playerTable, 1 do
			local pPlayer = playerTable[i]
			
			if (pPlayer ~= nil and musicTemplate ~= nil) then
				CreatureObject(pPlayer):playMusicMessage(musicTemplate)
				
				local pGhost = CreatureObject(pPlayer):getPlayerObject()
				if (pGhost ~= nil and not PlayerObject(pGhost):hasBadge(168)) then
					PlayerObject(pGhost):awardBadge(168)
				end
				
				if (faction == "CIS" and ThemeParkLogic:isInFaction(FACTIONIMPERIAL, pPlayer) == true) then
					return
				elseif (faction == "Republic" and ThemeParkLogic:isInFaction(FACTIONREBEL, pPlayer) == true) then
					return
				elseif (not ThemeParkLogic:isInFaction(FACTIONREBEL, pPlayer) == true and not ThemeParkLogic:isInFaction(FACTIONIMPERIAL, pPlayer) == true) then
					return
				end
				
				local pInventory = SceneObject(pPlayer):getSlottedObject("inventory")
				
				rngloot = 0 + getRandomNumber(0, 6)

				if (pInventory == nil or SceneObject(pInventory):isContainerFullRecursive()) then
					print("Inventory Full")
					CreatureObject(pPlayer):sendSystemMessage("Your inventory is full so you did not receive the event reward.")
				else
					if rngloot == 0 or rngloot == 6 then
						local crystalID = createLoot(pInventory, "power_crystals", 350, true)
						CreatureObject(pPlayer):sendSystemMessage("A mysterious crystal appears in your hand even as you start to forget the events you have just witnessed. You store it in your inventory.")
					end
					if rngloot == 1 then
						local crystalID = createLoot(pInventory, "krayt_tissue_rare", 336, true)
						CreatureObject(pPlayer):sendSystemMessage("A mysterious tissue appears in your hand even as you start to forget the events you have just witnessed. You store it in your inventory.")
					end
					if rngloot == 2 then
						local crystalID = createLoot(pInventory, "power_crystals", 300, true)
						CreatureObject(pPlayer):sendSystemMessage("A mysterious crystal appears in your hand even as you start to forget the events you have just witnessed. You store it in your inventory.")
					end
					if rngloot == 3 then
						local crystalID = createLoot(pInventory, "fire_breathing_spider", 126, true)
						CreatureObject(pPlayer):sendSystemMessage("A mysterious object appears in your hand even as you start to forget the events you have just witnessed. You store it in your inventory.")
					end
					if rngloot == 4 then
						local crystalID = createLoot(pInventory, "jedi_paintings", 1, true)
						CreatureObject(pPlayer):sendSystemMessage("A mysterious painting appears in your hand even as you start to forget the events you have just witnessed. You store it in your inventory.")
					end
					if rngloot == 5 then
						local crystalID = createLoot(pInventory, "dc15event", 1, true)
						CreatureObject(pPlayer):sendSystemMessage("A mysterious schematic appears in your hand even as you start to forget the events you have just witnessed. You store it in your inventory.")
					end		
				end

			end
			
		end
	end
end
