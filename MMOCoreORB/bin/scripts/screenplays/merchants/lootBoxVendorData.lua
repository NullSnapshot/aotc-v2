LootBoxVendorLogic = VendorLogic:new {
	scriptName = "LootBoxVendorLogic",
	currencies = {
	--For Tokens: Displayed Name, full template string (without shared_), if applicable: ScreenPlayData string, ScreenPlayData key
		{currency = "credits", name = "Credits", template = "", ScreenPlayDataString = "credits", ScreenPlayDataKey = "credits"},
	},
	--Displayed Name, full template string (without shared_), cost in {}, use the same structure as currencies
	merchandise = {
{name = "A Random Loot Crate", template = "loot_box_01", cost = {250000}},

	},
}

registerScreenPlay("LootBoxVendorLogic", false)
