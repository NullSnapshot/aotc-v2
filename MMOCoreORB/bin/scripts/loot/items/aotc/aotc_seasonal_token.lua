--Automatically generated by SWGEmu Spawn Tool v0.12 loot editor.

aotc_seasonal_token = {
	minimumLevel = 0,
	maximumLevel = -1,
	customObjectName = "",
	directObjectTemplate = "object/tangible/loot/misc/event_token_seasonal.iff",
	craftingValues = {},
	customizationStringNames = {},
	customizationValues = {}
}

addLootItemTemplate("aotc_seasonal_token", aotc_seasonal_token)
