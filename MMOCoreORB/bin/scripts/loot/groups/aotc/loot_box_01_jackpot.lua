--Automatically generated by SWGEmu Spawn Tool v0.12 loot editor.

loot_box_01_jackpot = {
	description = "",
	minimumLevel = 336,
	maximumLevel = 336,
	lootItems = {
		{itemTemplate = "xj6_schematic", weight = 1000000},
		{itemTemplate = "box_junk_5000", weight = 1000000},
		{itemTemplate = "stap_schematic", weight = 1000000},
		{itemTemplate = "deed_bunker", weight = 1000000},
		{itemTemplate = "deed_cloud", weight = 1000000},
		{itemTemplate = "deed_jabba", weight = 1000000},
		{itemTemplate = "deed_garage", weight = 1000000},
		{itemTemplate = "deed_YT", weight = 1000000},
		{itemTemplate = "deed_jedi_house", weight = 1000000},
		{itemTemplate = "deed_sith_house", weight = 1000000}
	}
}

addLootGroupTemplate("loot_box_01_jackpot", loot_box_01_jackpot)
