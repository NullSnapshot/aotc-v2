cis_sbd_restuss = Creature:new {
	objectName = "@mob/creature_names:cis_sbd",
	mobType = MOB_ANDROID,
	customName = "A Super Battle Droid",
	socialGroup = "rebel",
	faction = "rebel",
	level = 200,
	chanceHit = 0.33,
	damageMin = 290,
	damageMax = 300,
	baseXp = 2006,
	baseHAM = 10000,
	baseHAMmax = 16100,
	armor = 1,
	resists = {40,40,40,40,40,40,40,40,-1},
	meatType = "",
	meatAmount = 0,
	hideType = "",
	hideAmount = 0,
	boneType = "",
	boneAmount = 0,
	milk = 0,
	tamingChance = 0,
	ferocity = 0,
	pvpBitmask = ATTACKABLE,
	creatureBitmask = PACK + KILLER,
	optionsBitmask = AIENABLED + FACTIONAGGRO,
	diet = HERBIVORE,
	scale = 1.10,

	templates = {
			"object/mobile/death_watch_s_battle_droid_02.iff"
	},
	lootGroups = {
		{
			groups = {
				{group = "color_crystals", chance = 100000},
				{group = "junk", chance = 3400000},
				{group = "rifles", chance = 1200000},
				{group = "pistols", chance = 1200000},
				{group = "melee_weapons", chance = 1200000},
				{group = "carbines", chance = 1200000},
				{group = "restuss_cis", chance = 500000},
				{group = "stormtrooper_common", chance = 700000},
				{group = "wearables_common", chance = 500000}
			}
		}
	},
	defaultWeapon = "object/weapon/ranged/droid/droid_droideka_ranged.iff",
	conversationTemplate = "",
	reactionStf = "",
	personalityStf = "@hireling/hireling_military",
	attacks = merge(brawlermaster,marksmanmaster)
}

CreatureTemplates:addCreatureTemplate(cis_sbd_restuss, "cis_sbd_restuss")
